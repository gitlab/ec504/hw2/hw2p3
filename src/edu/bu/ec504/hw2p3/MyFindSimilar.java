package edu.bu.ec504.hw2p3;

import java.util.NoSuchElementException;

public class MyFindSimilar implements FindSimilar {

    /**
     * {@inheritDoc}
     */
    @Override
    public int addToDictionary(String entry) {
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndex(String entry) throws NoSuchElementException {
        throw new NotImplementedException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int closestWord(String word) {
        throw new NotImplementedException();
    }

    /**
     * Denotes that a method has not yet been implemented.
     */
    static public class NotImplementedException extends RuntimeException {}
}
